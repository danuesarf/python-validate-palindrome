import sys
import string

string = sys.argv[1]
stringrev = string[::-1]

if string == stringrev:
    print("Provided String \"%s\" is palindrome." % sys.argv[1])
else:
    print("Provided String \"%s\" is not a palindrome." % sys.argv[1])
