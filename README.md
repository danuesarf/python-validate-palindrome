To run this script, type
python3 script.py your_string

If you have space in the string, the sscript will only ready the first message

Example:
python3 script.py level
it will return "Provided String "level" is palindrome."

python3 script.py bash
it will return "Provided String "bash" is not a palindrome."
